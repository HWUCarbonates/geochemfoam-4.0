{
	//Transport step
	Info << "transport step" << endl;
   while (simple.correctNonOrthogonal())
    {
		forAll(solutionSpecies, i)
		{
			volScalarField& Yi = Y[i];
			dimensionedScalar DYi = DY[i];

			fvScalarMatrix YiEqn
			(
				fvm::ddt(Yi)
				+ fvm::div(phi, Yi, "div(phi,Yi)")
				- fvm::laplacian(DYi, Yi)
			);

			YiEqn.solve(mesh.solutionDict().solver("Yi"));
		}
	}

	//reaction step
	Info << "reaction step" << endl;
	rm->reactionStep(runTime.deltaT());
}