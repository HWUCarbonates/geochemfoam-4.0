#!/bin/bash

set -e

cp system/fvSolutionCCST system/fvSolution
cp 0/alpha1.org 0/alpha1
cp 0/T.org 0/T
blockMesh
setFields
interTransportFoam
