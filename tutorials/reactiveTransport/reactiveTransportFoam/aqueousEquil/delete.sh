#!/bin/bash

set -e

rm -rf 0.* 
rm -f constant/polyMesh/boundary
rm -f constant/polyMesh/*.gz 
