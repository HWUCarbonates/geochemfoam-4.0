# README #

This is GeoChemFoam, an OpenFOAM-based package developed to perform reactive multiphase transport simulation at the micro-scale

######

This code was developed at Heriot-Watt University.

Developers: Julien Maes, Saideep Pavuluri, Cyprien Soulaine

######

This is the version 4.0 

######
Instalation and running instruction can be find in the user guide in the doc folder

######

For information, contact Dr Julien Maes at j.maes@hw.ac.uk

Relevant papers:

Graveleau, M., Soulaine, C. and Tchelepi, H. A., "Pore-scale simulation of interphase multicomponent mass transfer for subsurface flow", Transport in porous media, 120(2), pp 287-308, 2017, https://link.springer.com/article/10.1007/s11242-017-0921-1

Soulaine, C. Roman, S., Kovscek, A. and Tchelepi, H. A., "Mineral dissolution and wormholing from a pore-scale perspective", Journal of fluid mechanics, 827, pp 457-483, 2017, https://www.cambridge.org/core/journals/journal-of-fluid-mechanics/article/mineral-dissolution-and-wormholing-from-a-porescale-perspective/82CE22EF080CA51DF1C2BEECA4C582DD 

Maes, J. and Geiger, S., "Direct pore-scale reactive transport modelling of wettability changes induced by surface complexation in carbonate rocks", Advances in Water Resources, 2111, pp 6-19, 2018, https://www.sciencedirect.com/science/article/pii/S0309170817306309?via%3Dihub

Maes, J. and Soulaine, C., "A novel compressive scheme to simulate species transfer across fluid interfaces unsing the Volume-Of-Fluid method", Chemical Engineering Sciences, 190, pp 405-418, 2018, https://www.sciencedirect.com/science/article/pii/S0009250918303944?via%3Dihub

Pavuluri, S., Maes, J. and Doster, F., "Spontaneous imbibition in a microchannel: analytical solution and assessment of volume of fluid formulations, Microfluidics and nanofluidics, 22, 2018, https://link.springer.com/article/10.1007%2Fs10404-018-2106-9

#######

OPENFOAM® is a registered trade mark of OpenCFD Limited, producer and distributor of the OpenFOAM software via www.openfoam.com.
